package ru.rencredit.jschool.kuzyushin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;

public class TaskCountCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-count";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Count all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[COUNT TASKS]");
        if (serviceLocator != null) {
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
            System.out.println("COUNT: " + serviceLocator.getTaskEndpoint().countAllTasks(sessionDTO));
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
