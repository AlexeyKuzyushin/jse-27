package ru.rencredit.jschool.kuzyushin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public class ProjectRemoveAllByUserId extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-remove-all-by-userId";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects by user";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE All PROJECT]");
        if (serviceLocator != null) {
            @Nullable SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
            serviceLocator.getProjectEndpoint().removeAllProjectsByUserId(sessionDTO);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
