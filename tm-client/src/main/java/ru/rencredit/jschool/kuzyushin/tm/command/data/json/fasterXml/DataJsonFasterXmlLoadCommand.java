package ru.rencredit.jschool.kuzyushin.tm.command.data.json.fasterXml;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Role;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;

public class DataJsonFasterXmlLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-fasterxml-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from json file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON LOAD]");
        if (serviceLocator != null) {
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
            serviceLocator.getDataEndpoint().loadDataJson(sessionDTO);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
