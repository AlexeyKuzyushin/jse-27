package ru.rencredit.jschool.kuzyushin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.sql.Date;
import java.text.ParseException;
import java.util.GregorianCalendar;

public class TaskUpdateFinishDate extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-update-finish-date";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update finish date of task";
    }

    @Override
    public void execute() throws ParseException, DatatypeConfigurationException {
        System.out.println("[UPDATE FINISH DATE]");
        System.out.println("ENTER ID:");
        if (serviceLocator != null) {
            @Nullable final String id = TerminalUtil.nextLine();
            System.out.println("ENTER DATE (dd-MMM-yyyy):");
            final Date newDate = Date.valueOf(TerminalUtil.nextLine());
            final GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(newDate);
            final XMLGregorianCalendar xmlGregDate =  DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
            serviceLocator.getTaskEndpoint().updateTaskFinishDate(sessionDTO, id, xmlGregDate);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
