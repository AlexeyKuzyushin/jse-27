package ru.rencredit.jschool.kuzyushin.tm.command.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;

public final class LogoutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Logout";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        if (serviceLocator != null) {
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
            serviceLocator.getSessionEndpoint().closeSession(sessionDTO);
            serviceLocator.getSessionService().clearCurrentSession();
        }
        System.out.println("[OK]");
    }
}
