package ru.rencredit.jschool.kuzyushin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;

public class TaskRemoveAllByUserId extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-remove-all-by-userId";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks of user";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        if (serviceLocator != null) {
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
            serviceLocator.getTaskEndpoint().removeAllTasksByUserId(sessionDTO);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
