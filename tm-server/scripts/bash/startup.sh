#!/bin/bash

echo "===> Startup tm-server"
echo ''
if [ -f tm-server.pid ]; then
	echo 'tm-server is work with pid '$(cat tm-server.pid)
	exit 1;
fi

mkdir -p ../bash/log
rm -f ../bash/log/tm-server.log
nohup java -jar ../../tm-server-1.0.0.jar > ../bash/log/tm-server.log 2>&1 &
echo $! > tm-server.pid
echo 'tm-server is running with pid '$!
