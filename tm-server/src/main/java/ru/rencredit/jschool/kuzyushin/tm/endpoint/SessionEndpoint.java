package ru.rencredit.jschool.kuzyushin.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.endpoint.ISessionEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.dto.Fail;
import ru.rencredit.jschool.kuzyushin.tm.dto.Result;
import ru.rencredit.jschool.kuzyushin.tm.dto.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.dto.Success;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;
import ru.rencredit.jschool.kuzyushin.tm.exception.user.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public final class SessionEndpoint implements ISessionEndpoint {

    private IServiceLocator serviceLocator;

    public SessionEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") final @Nullable String login,
            @WebParam(name = "password", partName = "password") final @Nullable String password) {
        return  serviceLocator.getSessionService().open(login, password);
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeSession(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(sessionDTO);
        try {
            serviceLocator.getSessionService().close(sessionDTO);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public Result closeAllUserSession(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(sessionDTO);
        try {
            serviceLocator.getSessionService().closeAll(sessionDTO);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }
}
