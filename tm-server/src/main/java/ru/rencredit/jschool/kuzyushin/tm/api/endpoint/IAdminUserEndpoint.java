package ru.rencredit.jschool.kuzyushin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.dto.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.dto.UserDTO;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import java.util.List;

public interface IAdminUserEndpoint {

    @NotNull
    List<UserDTO> findAllUsers(@Nullable SessionDTO session);

    void createUserWithEmail(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    void createUserWithRole(@Nullable SessionDTO sessionDTO, @Nullable String login,
                               @Nullable String password, @Nullable Role role) throws Exception;

    void lockUserByLogin(@Nullable SessionDTO sessionDTO, @Nullable String login) throws Exception;

    void unlockUserByLogin(@Nullable SessionDTO sessionDTO, @Nullable String login) throws Exception;

    void removeUserByLogin(@Nullable SessionDTO sessionDTO, @Nullable String login) throws Exception;

    void removeUserById(@Nullable SessionDTO sessionDTO, @Nullable String id) throws Exception;
}
