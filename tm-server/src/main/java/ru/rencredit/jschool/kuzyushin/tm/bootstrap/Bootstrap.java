package ru.rencredit.jschool.kuzyushin.tm.bootstrap;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.rencredit.jschool.kuzyushin.tm.api.endpoint.ISessionEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.api.service.*;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.*;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import ru.rencredit.jschool.kuzyushin.tm.service.*;

import javax.xml.ws.Endpoint;

public class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IUserService userService = new UserService(this);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(this);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(this);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(taskService, projectService, userService);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ISqlConnectionService sqlConnectionService = new SqlConnectionService(this);

    @Getter
    @NotNull
    private final IDataService dataService = new DataService(this);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final DataEndpoint dataEndpoint = new DataEndpoint(this);

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    public void run(){
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        initEndpoint();
    }

    private void initEndpoint() {
        registry(taskEndpoint);
        registry(projectEndpoint);
        registry(userEndpoint);
        registry(sessionEndpoint);
        registry(dataEndpoint);
        registry(adminUserEndpoint);
    }

    private void registry(final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServerHost();
        final Integer port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }
}
