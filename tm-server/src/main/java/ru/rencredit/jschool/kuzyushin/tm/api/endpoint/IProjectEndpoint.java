package ru.rencredit.jschool.kuzyushin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.dto.ProjectDTO;
import ru.rencredit.jschool.kuzyushin.tm.dto.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;

import java.util.Date;
import java.util.List;

public interface IProjectEndpoint {

    Long countAllProjects(@Nullable SessionDTO sessionDTO);

    @NotNull
    List<ProjectDTO> findAllProjects(@Nullable SessionDTO sessionDTO);

    @NotNull
    List<ProjectDTO> findAllProjectsByUserId(@Nullable SessionDTO sessionDTO);

    void clearProjects(@Nullable SessionDTO sessionDTO) throws Exception;

    void createProject(@Nullable SessionDTO sessionDTO, @Nullable String name, @Nullable String description);

    @Nullable
    ProjectDTO findProjectById(@Nullable SessionDTO sessionDTO, @Nullable String id);

    @Nullable
    ProjectDTO findProjectByName(@Nullable SessionDTO sessionDTO, @Nullable String name);

    void removeAllProjectsByUserId(@Nullable SessionDTO sessionDTO);

    void removeProjectById(@Nullable SessionDTO sessionDTO, @Nullable String id);

    void removeProjectByName(@Nullable SessionDTO sessionDTO, @Nullable String name);

    @NotNull
    ProjectDTO updateProjectById(@Nullable SessionDTO sessionDTO, @Nullable String id,
                           @Nullable String name, @Nullable String description);

    void updateProjectStartDate(@Nullable SessionDTO sessionDTO, @Nullable String id, @Nullable Date date);

    void updateProjectFinishDate(@Nullable SessionDTO sessionDTO, @Nullable String id, @Nullable Date date);
}
