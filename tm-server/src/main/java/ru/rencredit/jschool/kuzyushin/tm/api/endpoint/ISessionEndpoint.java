package ru.rencredit.jschool.kuzyushin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.dto.Result;
import ru.rencredit.jschool.kuzyushin.tm.dto.SessionDTO;

public interface ISessionEndpoint {

    @Nullable
    SessionDTO openSession(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    Result closeSession(@Nullable SessionDTO sessionDTO);

    @NotNull
    Result closeAllUserSession(@Nullable SessionDTO sessionDTO);
}
