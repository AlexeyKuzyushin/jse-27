package ru.rencredit.jschool.kuzyushin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @NotNull
    Long count();

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAll(@NotNull String userId);

    @NotNull
    List<Task> findAll(@NotNull String userId, @NotNull String projectId);

    @Nullable
    Task findById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task findByName(@NotNull String userId, @NotNull String name);

    void clear();

    void removeAll(@NotNull String userId);

    void removeById(@NotNull String userId, @NotNull String id);

    void removeByName(@NotNull String userId, @NotNull String name);

    void removeAllByProjectId(@NotNull String projectId);
}
