package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.ISessionRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IPropertyService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.dto.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.dto.UserDTO;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.*;
import ru.rencredit.jschool.kuzyushin.tm.exception.user.AccessDeniedException;
import ru.rencredit.jschool.kuzyushin.tm.repository.SessionRepository;
import ru.rencredit.jschool.kuzyushin.tm.util.HashUtil;
import ru.rencredit.jschool.kuzyushin.tm.util.SignatureUtil;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    public SessionService(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public void remove(final @NotNull Session session) {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();

        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.remove(session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void persist(final @Nullable Session session) {
        if (session == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.persist(session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void merge(final @Nullable Session session) {
        if (session == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.merge(session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public SessionDTO open(final @Nullable String login, final @Nullable String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return null;
        final Session session = new Session();
        session.setUser(user);
        session.setTimestamp(System.currentTimeMillis());
        final IPropertyService propertyService = serviceLocator.getPropertyService();
        final String salt = propertyService.getSessionSalt();
        final Integer cycle = propertyService.getSessionCycle();
        session.setSignature(SignatureUtil.sign(SessionDTO.toDTO(session),salt, cycle));
        persist(session);
        return SessionDTO.toDTO(session);
    }

    @Override
    @Nullable
    public  SessionDTO sign(final @Nullable SessionDTO sessionDTO) {
        if (sessionDTO == null) return null;
        sessionDTO.setSignature(null);
        final IPropertyService propertyService = serviceLocator.getPropertyService();
        final String salt = propertyService.getSessionSalt();
        final Integer cycle = propertyService.getSessionCycle();
        final String signature = SignatureUtil.sign(sessionDTO, salt, cycle);
        sessionDTO.setSignature(signature);
        return sessionDTO;
    }

    @Override
    @Nullable
    public UserDTO getUser(final @Nullable SessionDTO sessionDTO) {
        final String userId = getUserId(sessionDTO);
        return UserDTO.toDTO(serviceLocator.getUserService().findById(userId));
    }

    @Override
    @Nullable
    public String getUserId(final @Nullable SessionDTO sessionDTO) {
        validate(sessionDTO);
        return sessionDTO.getUserId();
    }

    @NotNull
    @Override
    public  List<SessionDTO> getListSession(final @Nullable SessionDTO sessionDTO) {
        validate(sessionDTO);
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        @NotNull final List<SessionDTO> sessionsDTO = SessionDTO.toDTO(repository.findAll(sessionDTO.getUserId()));
        entityManager.close();
        return sessionsDTO;
    }

    @Override
    public boolean checkDataAccess(final @Nullable String login, final @Nullable String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return false;
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    public boolean isValid(final @Nullable SessionDTO sessionDTO) {
        try {
            validate(sessionDTO);
            return true;
        } catch (AccessDeniedException e) {
            return false;
        }
    }

    @Override
    public void close(final @Nullable SessionDTO sessionDTO){
        validate(sessionDTO);
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeById(sessionDTO.getId());
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void closeAll(@Nullable SessionDTO sessionDTO) {
        validate(sessionDTO);
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeAll(sessionDTO.getUserId());
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void validate(@Nullable SessionDTO sessionDTO) {
        if (sessionDTO == null) throw new AccessDeniedException();
        if (sessionDTO.getSignature() == null ||
                sessionDTO.getSignature().isEmpty()) throw new AccessDeniedException();
        if (sessionDTO.getUserId() == null ||
                sessionDTO.getUserId().isEmpty()) throw new EmptyUserIdException();
        if (sessionDTO.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final SessionDTO temp = sessionDTO.clone();
        if (temp == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = sessionDTO.getSignature();
        @Nullable final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        final boolean isContain = repository.contains(sessionDTO.getId());
        entityManager.close();
        if (!isContain) throw new AccessDeniedException();
    }

    @Override
    public void validate(@Nullable SessionDTO sessionDTO, Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(sessionDTO);
        final String userId = sessionDTO.getUserId();
        final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new EmptyUserException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    public void signOutByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new AccessDeniedException();
        @Nullable final UserDTO userDTO = UserDTO.toDTO(serviceLocator.getUserService().findByLogin(login));
        if (userDTO == null) throw new AccessDeniedException();
        @NotNull final String userId = userDTO.getId();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void singOutByUserId(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }
}
