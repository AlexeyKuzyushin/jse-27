package ru.rencredit.jschool.kuzyushin.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.endpoint.IProjectEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.dto.ProjectDTO;
import ru.rencredit.jschool.kuzyushin.tm.dto.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

@WebService
@NoArgsConstructor
public final class ProjectEndpoint implements IProjectEndpoint {

    private IServiceLocator serviceLocator;

    public ProjectEndpoint(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public Long countAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO) {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        return serviceLocator.getProjectService().count();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public List<ProjectDTO> findAllProjects(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        return serviceLocator.getProjectService().findAll();
    }

    @NotNull
    @Override
    @WebMethod
    public  List<ProjectDTO> findAllProjectsByUserId(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getProjectService().findAllByUserId(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public void clearProjects(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        serviceLocator.getProjectService().clear();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void createProject(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") final @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().create(sessionDTO.getUserId(), name, description);
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO findProjectById(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") final @Nullable String id) {
        return ProjectDTO.toDTO(serviceLocator.getProjectService().findById(sessionDTO.getUserId(), id));
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO findProjectByName(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") final @Nullable String name) {
        return ProjectDTO.toDTO(serviceLocator.getProjectService().findByName(sessionDTO.getUserId(), name));
    }

    @Override
    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") final @Nullable String id) {
        serviceLocator.getProjectService().removeById(sessionDTO.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "name", partName = "name") final @Nullable String name) {
        serviceLocator.getProjectService().removeByName(sessionDTO.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeAllProjectsByUserId(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().removeAllByUserId(sessionDTO.getUserId());
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO updateProjectById(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") final @Nullable String id,
            @WebParam(name = "name", partName = "name") final @Nullable String name,
            @WebParam(name = "description", partName = "description") final @Nullable String description) {
        return ProjectDTO.toDTO(serviceLocator.getProjectService().updateById(sessionDTO.getUserId(), id,
                name, description));
    }

    @Override
    @WebMethod
    public void updateProjectStartDate(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") final @Nullable String id,
            @WebParam(name = "date", partName = "date") @Nullable final Date date) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().updateFinishDate(sessionDTO.getUserId(), id, date);
    }

    @Override
    @WebMethod
    public void updateProjectFinishDate(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") final @Nullable String id,
            @WebParam(name = "date", partName = "date") final @Nullable Date date) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getProjectService().updateFinishDate(sessionDTO.getUserId(), id, date);
    }
}
