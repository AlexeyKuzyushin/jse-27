package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.ITaskRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ITaskService;
import ru.rencredit.jschool.kuzyushin.tm.dto.TaskDTO;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.*;
import ru.rencredit.jschool.kuzyushin.tm.exception.system.IncorrectStartDateException;
import ru.rencredit.jschool.kuzyushin.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {

    public TaskService(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    public Long count() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Long countOfTasks = repository.count();
        entityManager.close();
        return countOfTasks;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<TaskDTO> tasksDTO = TaskDTO.toDTO(repository.findAll());
        entityManager.close();
        return tasksDTO;
    }

    @NotNull
    @Override
    public  List<TaskDTO> findAll(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<TaskDTO> tasksDTO = TaskDTO.toDTO(repository.findAll(userId));
        entityManager.close();
        return tasksDTO;
    }

    @Override
    public @NotNull List<TaskDTO> findAll(final @Nullable String userId, final @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<TaskDTO> tasksDTO = TaskDTO.toDTO(repository.findAll(userId));
        entityManager.close();
        return tasksDTO;
    }

    @Override
    public void create(final @Nullable String userId, final @Nullable String projectId,
                       final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(serviceLocator.getUserService().findById(userId));
        task.setProject(serviceLocator.getProjectService().findById(userId, projectId));
        persist(task);
    }

    @Nullable
    @Override
    public Task findById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @Nullable final Task task = repository.findById(userId, id);
        entityManager.close();
        return task;
    }

    @Nullable
    @Override
    public Task findByName(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @Nullable final Task task = repository.findByName(userId, name);
        entityManager.close();
        return task;
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAllByUserId(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeAll(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByName(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAllByProjectId(@Nullable String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeAllByProjectId(projectId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Task updateById(
            final @Nullable String userId, final @Nullable String id,
            final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @Nullable Task task = findById(userId, id);
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public void updateStartDate(final @Nullable String userId, final @Nullable String id, final @Nullable Date date) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @Nullable final Task task = findById(userId, id);
        if (date.before(new Date(System.currentTimeMillis()))) throw new IncorrectStartDateException(date);
        task.setStartDate(date);
        merge(task);
    }

    @Override
    public void updateFinishDate(final @Nullable String userId, final @Nullable String id, final @Nullable Date date) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @Nullable final Task task = findById(userId, id);
        assert task != null;
        if (task.getStartDate() == null) throw new IncorrectStartDateException();
        if (task.getStartDate().after(date)) throw new IncorrectStartDateException(date);
        task.setFinishDate(date);
        merge(task);

    }

    @Override
    public void remove(final @NotNull Task task) {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.remove(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void persist(final @Nullable Task task) {
        if (task == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.persist(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void merge(final @Nullable Task task) {
        if (task == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.merge(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void load(final @Nullable List<TaskDTO> tasks) {
        if (tasks == null) return;
        clear();
        for (final TaskDTO taskDTO: tasks){
            @NotNull final Task task = new Task();
            task.setName(taskDTO.getName());
            task.setDescription(taskDTO.getDescription());
            task.setUser(serviceLocator.getUserService().findById(taskDTO.getUserId()));
            task.setProject(serviceLocator.getProjectService().findById(taskDTO.getUserId(), taskDTO.getProjectId()));
            task.setId(taskDTO.getId());
            task.setStartDate(taskDTO.getStartDate());
            task.setFinishDate(taskDTO.getFinishDate());
            task.setCreationTime(taskDTO.getCreationDate());
            persist(task);
        }
    }
}
