package ru.rencredit.jschool.kuzyushin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    Long count();

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAll(@NotNull String userId);

    @Nullable
    Project findById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findByName(@NotNull String userId, @NotNull String name);

    void clear();

    void removeAll(@NotNull String userId);

    void removeById(@NotNull String userId, @NotNull String id);

    void removeByName(@NotNull String userId, @NotNull String name);
}
